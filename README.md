# linux-softwares

## Ubuntu 18.04
1. Télécharger l'image http://releases.ubuntu.com/18.04.1/ubuntu-18.04.1-desktop-amd64.iso
2. Faire une clef linux bootable avec le logiciel Créateur d'image de démarrage linux


## Docker
Suivre ce readme là pour installer docker et docker-compose https://gitlab.com/simplonlyon/P6/Docker-intro/blob/master/readme.md

## PHP / Composer
1. Ajouter le repository `sudo add-apt-repository ppa:ondrej/php`
2. `sudo apt update`
3. Installer php-fpm et ses packages : `sudo apt install php7.2 php7.2-cli php7.2-common php7.2-curl php7.2-fpm php7.2-json php7.2-mbstring php7.2-mysql php7.2-opcache php7.2-readline php7.2-xml php7.2-zip`
4. Télécharger composer `curl https://getcomposer.org/composer.phar --output composer.phar` (si curl n'est pas installé `sudo apt install curl`)
5. Déplacer composer `sudo mv composer.phar /usr/local/bin/composer`
6. Changer les droits `sudo chown $USER:$USER /usr/local/bin/composer`
7. `sudo chmod 755 /usr/local/bin/composer`


## Visual Studio Code
  * `curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg`
  * `sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/`
  * `sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'`
  * `sudo apt-get install apt-transport-https`
  * `sudo apt-get update`
  * `sudo apt-get install code`
 Les extensions à installer dans vscode : 
* Docker
* Angular Language Service
* VS Live Share
* PHP Intelephense
* TWIG Pack

## Git
Configurer les email et username de git : 
* `git config --global user.name "votre username gitlab"`
* `git config --global user.email "votre email gitlab"`

Créer une pair clef privée/public : ` ssh-keygen -t ed25519 -C "votre email gitlab"` . Vous pouvez ensuite faire `cat /home/$USER/.ssh/id_rsa.pub` pour afficher votre clef publique dans le terminal, c'est cette clef qu'il faut ajouter dans votre compte comme ici https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html

## Discord
1. Télécharger le .deb https://discordapp.com/api/download?platform=linux&format=deb
2. Ouvrir un terminal et se mettre dans dans le dossier Téléchargements puis faire un `sudo dpkg -i fichier-téléchargé.deb` (bien remplacer le .deb de la commande par le bon nom de fichier)

## Npm / Node
1. Ajouter le dépôt `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`
2. `sudo apt update`
3. Installer node `sudo apt install nodejs`

Les packages npm qu'on a installés en global avec `sudo npm install -d nom-du-package` :
* @angular/cli
* typescript
* nativescript

## Mysql Workbench
`sudo apt install mysql-workbench`